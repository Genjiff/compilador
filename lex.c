/*
Luana Roberta da Silva
MATA61 - Compiladores

- Fontes de pesquisa/estudo
Livro: Lex & Yacc by John R. Levine, Tony Mason and Doug Brown
Video-aula: "Part 01: Tutorial on lex/yacc" de Jonathan Engelsma
*/


#include <stdio.h>
#include "lex.h"

extern int yylex();
extern int yylineno;
extern char* yytext;

/*char *reservedWords[] = {NULL, "and", "not", "or", "elseif", "while", "do", "function", "end", "for", "else", "if", "then", "return", "local", "nil"};
char *otherItens[] = {"+", "-", "*", "/", ",", "(", ")", ";", "=", "==", "~=", "<=", ">=", "<", ">"};
*/

int main(int argc, char *argv[])
{

	if (argc != 3){
		printf("Erro. A entrada deve ter o seguinte formato: ./arquivo prog.l prog.s");
		return 1;
	}

 	FILE *output = fopen(argv[2], "w");
	FILE *input = fopen(argv[1], "r");
	extern FILE *yyin;
	yyin = input;

	int ntoken, vtoken;

	ntoken = yylex();
	while(ntoken) {

		switch(ntoken){
			case ELSE:
			case IF:
			case INT:
			case RETURN:
			case VOID:
			case WHILE:
				fprintf(output, "(%d, KEY, \"%s\")\n", yylineno, yytext);
				break;

			case PLUS:
			case MINUS:
			case TIMES:
			case DIV:
			case LT:
			case LTEQ:
			case GT:
			case GTEQ:
			case EQ:
			case NEQ:
			case ASSIGN:
			case SEMICOL:
			case COMMA:
			case OPENPAR:
			case CLOSEPAR:
			case OPENBRACK:
			case CLOSEBRACK:
			case OPENBRACE:
			case CLOSEBRACE:
				fprintf(output, "(%d, SYM, \"%s\")\n", yylineno, yytext);
				break;

			//NUMERO - INTEGER
			case NUM:
				fprintf(output, "(%d, NUM, \"%s\")\n", yylineno, yytext);
				break;

			//IDENTIFICADORES
			case ID:
				fprintf(output, "(%d, ID, \"%s\")\n", yylineno, yytext);
				break;

			//COMENTARIO
			case STARTCOMMENT:
				while (ntoken != ENDCOMMENT){
					ntoken = yylex();
				}
				break;

			//se nao for nenhum dos acimas, erro.
			case ERROR:
				printf("Erro na linha %d, em \"%s\" \n", yylineno, yytext);
				return 1;



		}
		// pega o proximo token
		ntoken = yylex();
	}
	return 0;
}
