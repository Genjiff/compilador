#define ELSE 1
#define IF 2
#define INT 3
#define RETURN 4
#define VOID 5
#define WHILE 6
#define PLUS 7
#define MINUS 8
#define TIMES 9
#define DIV 10
#define LT 11
#define LTEQ 12
#define GT 13
#define GTEQ 14
#define EQ 15
#define NEQ 16
#define ASSIGN 17
#define SEMICOL 18
#define COMMA 19
#define OPENPAR 20
#define CLOSEPAR 21
#define OPENBRACK 22
#define CLOSEBRACK 23
#define OPENBRACE 24
#define CLOSEBRACE 25

#define ID 26

#define NUM 27

#define ERROR 28

#define STARTCOMMENT 29
#define ENDCOMMENT 30
#define BREAKLINE 31
