%{
#include "lex.h"
%}
%option nounput yylineno

%%
"else"			return ELSE;
"if"			return IF;
"int"			return INT;
"return"		return RETURN;
"void"			return VOID;
"while"			return WHILE;

"+" 			return PLUS;
"-"				return MINUS;
"*"				return TIMES;
"/"				return DIV;
"<"				return LT;
"<="			return LTEQ;
">"				return GT;
">="			return GTEQ;
"=="			return EQ;
"!="			return NEQ;
"="				return ASSIGN;
";"				return SEMICOL;
","				return COMMA;
"("				return OPENPAR;
")"				return CLOSEPAR;
"["				return OPENBRACK;
"]"				return CLOSEBRACK;
"{"				return OPENBRACE;
"}"				return CLOSEBRACE;

[a-z][a-z0-9A-Z]* 		return ID;
[0-9][0-9]*				return NUM;
[ \t]				    ;
[\n]			return BREAKLINE;
"/*"			return STARTCOMMENT;
"*/"			return ENDCOMMENT;
.				return ERROR;


%%

int yywrap(void)
{
	return 1;
}
